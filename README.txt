Summary
==============
A simple module that view list of users birthday and optional sends
notifications about users birthday to the specified e-mail address.

You can:
- View list of users with filled birthday field.
- Configure module to send notification about user birthdays in specific period.

Dependencies
==============
Date module with enabled Date Popup submodule: https://drupal.org/project/date

Installation
==============
1. Put files into your modules directory.
2. Enable module and go to "admin/config/people/birthdayinform" to configure.

Configuration
===================
1. Select date format. If you want add custom format date,
go to: "admin/config/regional/date-time".

2. Check "Send notification about users birthdays via e-mail" if you want
receive e-mails with information about site users birthdays.

3. Select days of the week on which the system will look for upcoming birthdays
and will send e-mail.

4. Type number of next days to check. Module will check this number of next days
on every day that is selected in previous step.

5. Fill "Send notification to the following email address" field.
