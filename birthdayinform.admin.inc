<?php
/**
 * @file
 * Implementing module settings form.
 */

/**
 * Admin settings callback.
 */
function birthdayinform_admin_settings($form, &$form_state) {
  $form = array();

  // Get date types and formats.
  $format_types = system_get_date_types();
  $date_formats = array();
  foreach ($format_types as $format_type) {
    $type = $format_type['type'];
    $format = variable_get('date_format_' . $type, '');
    $date_formats[$type] = $format_type['title'] . ': ' . format_date(REQUEST_TIME, $type, $format);
  }

  $form['birthdayinform_date_format'] = array(
    '#type'          => 'select',
    '#title'         => t('Date format'),
    '#options'       => $date_formats,
    '#default_value' => variable_get('birthdayinform_date_format', 'short'),
    '#description'   => t('If you want  add custom format date, go to:') . ' ' . l(t('date and time settings'), 'admin/config/regional/date-time'),
  );
  $form['birthdayinform_send_mail_info'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Send notification about users birthdays via e-mail'),
    '#default_value' => variable_get('birthdayinform_send_mail_info', '0'),
  );
  $form['checking_config'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array('input[name="birthdayinform_send_mail_info"]' => array('checked' => TRUE)),
    ),
  );
  $form['checking_config']['days_of_week'] = array(
    '#type' => 'fieldset',
    '#title' => t('Days of the week on which the system will look for upcoming birthdays and will send e-mail'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  foreach (birthdayinform_get_days_of_week_array() as $number => $day) {
    $form['checking_config']['days_of_week']['birthdayinform_checking_day_' . $number] = array(
      '#type'          => 'checkbox',
      '#title'         => $day['name'],
      '#default_value' => $day['state'],
    );
  }

  $form['checking_config']['birthdayinform_number_of_days'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Number of next days to check'),
    '#default_value' => variable_get('birthdayinform_number_of_days', 0),
    '#element_validate' => array('birthdayinform_settings_element_validate'),
  );
  $form['checking_config']['birthdayinform_email'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Send notification to the following email address'),
    '#default_value' => variable_get('birthdayinform_email', variable_get('site_mail', '')),
    '#element_validate' => array('birthdayinform_settings_element_validate'),
  );
  $form['checking_config']['birthdayinform_last_send'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Date of the last notification sending'),
    '#default_value' => variable_get('birthdayinform_last_send', NULL),
    '#description'   => t('This date is required to avoid multiple notification sending'),
  );

  return system_settings_form($form);
}

/**
 * Settings form element validate.
 */
function birthdayinform_settings_element_validate($element, &$form_state, $form) {
  if (empty($element['#value'])) {
    $message = t('@name field is required.', array('@name' => $element['#title']));
  }
  elseif ($element['#name'] == 'birthdayinform_number_of_days' && !is_numeric($element['#value'])) {
    $message = t('@name field value must be numeric.', array('@name' => $element['#title']));
  }
  elseif ($element['#name'] == 'birthdayinform_email' && !valid_email_address($element['#value'])) {
    $message = t('@name field value must contain valid e-mail address.', array('@name' => $element['#title']));
  }

  if (isset($message)) {
    $form_state['complete form']['checking_config']['#states']['visible'] = TRUE;
    form_error($element, check_plain($message));
  }
}
